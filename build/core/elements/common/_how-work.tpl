<div class="how-work">
    <div class="how-work__wrap">
        <div class="container">
            <div class="how-work__header">Как мы работаем</div><!-- /.how-work__header -->
            <div class="how-work__main">
                <div class="how-work__item">
                    <div class="how-work__icon">
                        <svg class="ico-svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#how__icon_01" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </div>
                    <div class="how-work__title">Выбираете проект</div>
                    <div class="how-work__text">Присылаете свой на расчет или выбираете в нашем каталоге.</div>
                </div><!-- /.how-work__item -->
                <div class="how-work__item">
                    <div class="how-work__icon">
                        <svg class="ico-svg" viewBox="-34 -18 585 585.335"  xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#how__icon_02" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </div>
                    <div class="how-work__title">Оставляете заявку</div>
                    <div class="how-work__text">Телефон, Viber, WhatsApp - <a href="tel:89002000000" class="tel">8 900 200-00-00</a> или написать на почту
                        <a href="mailto:brus-doma@mail.ru">brus-doma@mail.ru</a></div>
                </div><!-- /.how-work__item -->
                <div class="how-work__item">
                    <div class="how-work__icon">
                        <svg class="ico-svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#how__icon_03" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </div>
                    <div class="how-work__title">Заключаем договор</div>
                    <div class="how-work__text">Без предоплат. Подписать можем удаленно или бесплатный выезд специалиста к вам на встречу</div>
                </div><!-- /.how-work__item -->
                <div class="how-work__item">
                    <div class="how-work__icon">
                        <svg class="ico-svg" viewBox="0 -65 427.366 427"  xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#how__icon_04" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </div>
                    <div class="how-work__title">Заготовка материалов</div>
                    <div class="how-work__text">Готовим комплекта материала для вашего будущего дома.</div>
                </div><!-- /.how-work__item -->
                <div class="how-work__item">
                    <div class="how-work__icon">
                        <svg class="ico-svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#how__icon_05" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </div>
                    <div class="how-work__title">Строительство дома</div>
                    <div class="how-work__text">Поставка материала на участок и начало строительство дома.</div>
                </div><!-- /.how-work__item -->
                <div class="how-work__item">
                    <div class="how-work__icon">
                        <svg class="ico-svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#how__icon_06" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </div>
                    <div class="how-work__title">Сдача дома</div>
                    <div class="how-work__text">Подписание Акта выполненных работ. Ожидание усадки.</div>
                </div><!-- /.how-work__item -->
            </div><!-- /.how-work__main -->
        </div><!--/.container -->
    </div>
</div><!-- /.how-work -->
