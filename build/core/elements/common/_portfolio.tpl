<div class="portfolio">
    <div class="container">
        <div class="portfolio__row">
            <div class="portfolio__content">
                <div class="portfolio__header">Мы построили</div>
                <div class="portfolio__text">
                    <p>С 2009 года мы построили более 1500 объектов. Опыт наших бригад колоссален.</p>
                    <p>В среднем одна бригада строит 2 дома в месяц. 30% заказчиков приходят по рекомендациям!</p>
                </div>
                <div class="portfolio__nav">
                    <button class="portfolio__button portfolio__button--prev portfolio-nav-prev">
                        <i>
                            <svg class="ico-svg" viewBox="0 0 476.213 476.213"  xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="assets/template/images/sprites/sprite-icons.svg#right-arrow" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </button>
                    <button class="portfolio__button portfolio__button--next portfolio-nav-next">
                        <i>
                            <svg class="ico-svg" viewBox="0 0 476.213 476.213"  xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="assets/template/images/sprites/sprite-icons.svg#right-arrow" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </button>
                </div>
            </div><!--/.portfolio__content -->
            <div class="portfolio__slider">
                <div class="swiper-container portfolio-slider">
                    <div class="swiper-wrapper">

                        {'!pdoResources' | snippet : [
                        'parents' => '13',
                        'hideContainers' => '1',
                        'sortby' => 'menuindex',
                        'includeTVs' => 'portfolio_preview,portfolio_date',
                        'limit' => '10',
                        'tpl' => 'tpl.portfolio',
                        ]}

                    </div><!--/.swiper-wrapper -->
                </div><!--/.swiper-container -->
            </div><!--/.portfolio__slider -->
        </div><!--/.portfolio__row -->
    </div><!--/.container -->
</div><!--/.portfolio -->
