{extends 'file:templates/common/_layout.tpl'}

{block 'heading'}
    <div class="primary" style="background-image: url('assets/template/images/primary__bg.webp')">
        <div class="primary__slider">
            <div class="swiper-container primary-slider">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="container">
                            <div class="primary__item">
                                <div class="primary__wrap">
                                    <div class="primary__header">Строительство <br/>домов из бруса</div>
                                    <ul class="primary__progress">
                                        <li>
                                            <div class="primary__progress--item">Коренные костромские мастера</div>
                                        </li>
                                        <li>
                                            <div class="primary__progress--item">Построили более 1500 объектов</div>
                                        </li>
                                        <li>
                                            <div class="primary__progress--item">Строим соблюдая технологию на деревянные нагели</div>
                                        </li>
                                    </ul>
                                    <div class="primary__lg">
                                        <div class="primary__link">
                                            <span>Смотреть проекты</span>
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 192 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="assets/template/images/sprites/sprite-icons.svg#angle-right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </div>
                                </div>
                                <div class="primary__sm">
                                    <div class="primary__link">
                                        <span>Смотреть проекты</span>
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 192 512"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="assets/template/images/sprites/sprite-icons.svg#angle-right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--/.swiper-slide -->
                    <div class="swiper-slide">
                        <div class="container">
                            <div class="primary__item">
                                <div class="primary__wrap">
                                    <div class="primary__header">Строительство <br/>бань из бруса</div>
                                    <ul class="primary__progress">
                                        <li>
                                            <div class="primary__progress--item">Коренные костромские мастера</div>
                                        </li>
                                        <li>
                                            <div class="primary__progress--item">Построили более 1500 объектов</div>
                                        </li>
                                        <li>
                                            <div class="primary__progress--item">Строим соблюдая технологию на деревянные нагели</div>
                                        </li>
                                    </ul>
                                    <div class="primary__lg">
                                        <div class="primary__link">
                                            <span>Смотреть проекты</span>
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 192 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="assets/template/images/sprites/sprite-icons.svg#angle-right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </div>
                                </div>
                                <div class="primary__sm">
                                    <div class="primary__link">
                                        <span>Смотреть проекты</span>
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 192 512"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="assets/template/images/sprites/sprite-icons.svg#angle-right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--/.swiper-slide -->
                    <div class="swiper-slide">
                        <div class="container">
                            <div class="primary__item">
                                <div class="primary__wrap">
                                    <div class="primary__header">В строительстве <br/>с 2009 года</div>
                                    <ul class="primary__progress">
                                        <li>
                                            <div class="primary__progress--item">Коренные костромские мастера</div>
                                        </li>
                                        <li>
                                            <div class="primary__progress--item">Построили более 1500 объектов</div>
                                        </li>
                                        <li>
                                            <div class="primary__progress--item">Строим соблюдая технологию на деревянные нагели</div>
                                        </li>
                                    </ul>
                                    <div class="primary__lg">
                                        <div class="primary__link">
                                            <span>Смотреть проекты</span>
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 192 512"  xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="assets/template/images/sprites/sprite-icons.svg#angle-right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </div>
                                </div>
                                <div class="primary__sm">
                                    <div class="primary__link">
                                        <span>Смотреть проекты</span>
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 192 512"  xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="assets/template/images/sprites/sprite-icons.svg#angle-right" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--/.swiper-slide -->
                </div>
            </div><!--/.primary-slider -->
        </div><!--/.primary__slider -->
        <div class="primary__nav">
            <div class="control">
                <div class="control__count">
                    <div class="control__count--active primary-counter">01</div>
                    <div class="control__count--divider">/</div>
                    <div class="control__count--total">03</div>
                </div>
                <div class="control__nav">
                    <button class="control__button control__button--prev primary-nav-prev">
                        <i>
                            <svg class="ico-svg" viewBox="0 0 476.213 476.213" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="assets/template/images/sprites/sprite-icons.svg#right-arrow" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </button>
                    <button class="control__button control__button--next primary-nav-next">
                        <i>
                            <svg class="ico-svg" viewBox="0 0 476.213 476.213" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="assets/template/images/sprites/sprite-icons.svg#right-arrow" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </button>
                </div>
            </div>
        </div>
        <div class="primary__footer">
            <div class="container">
                <div class="primary__footer--wrap">
                    <div class="primary__switch">
                        <div class="primary__switch--item slide-item-0 active" data-slide="1">
                            <strong>01.</strong>
                            <span>Дома из бруса <br/>от 310 000 руб.</span>
                        </div><!--/.primary__switch--item -->
                        <div class="primary__switch--item slide-item-1" data-slide="2">
                            <strong>02.</strong>
                            <span>Бани из бруса <br/>от 270 000 руб.</span>
                        </div><!--/.primary__switch--item -->
                        <div class="primary__switch--item slide-item-2" data-slide="3">
                            <strong>03.</strong>
                            <span>В строительстве <br/>с 2009 года</span>
                        </div><!--/.primary__switch--item -->
                    </div><!--/.primary__switch -->
                    <div class="primary__lines"></div><!--/.primary__switch -->
                </div>
            </div>
        </div><!--/.primary__footer -->
    </div><!--/.primary -->
{/block}

{block 'main'}

    <div class="hits">
        <div class="container">
            <div class="hits__main">
                <div class="hits__elem hits__header">
                    <div class="hits__header--wrap">
                        <div class="hits__header--title">Хиты <br/>продаж</div>
                        <div class="hits__header--arrow">
                            <svg class="ico-svg" viewBox="0 0 476.213 476.213" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="assets/template/images/sprites/sprite-icons.svg#right-arrow" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </div>
                    </div>
                </div><!--/.hits__header -->
                {$_modx->runSnippet('!msProducts', [
                    'parents' => 11,
                    'includeThumbs' => 'small',
                    'limit' => 12,
                    'sortby' => 'RAND()',
                    'tpl' => 'tpl.hits'
                ])}
                <a href="#" class="hits__elem hits__view">
                    <div class="hits__view--block">
                        <div class="hits__view--text">Смотреть полный каталог</div>
                        <div class="hits__view--arrow">
                            <svg class="ico-svg" viewBox="0 0 476.213 476.213" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="assets/template/images/sprites/sprite-icons.svg#right-arrow" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </div>
                    </div>
                </a><!--/.hits__view -->
            </div><!--/.hits__main -->
        </div><!--/.container -->
    </div><!--/.hits -->

    <div class="order">
        <div class="container">
            <div class="order__wrap">
                <div class="order__content">
                    <div class="order__header">Подайте заявку на  <br/>бесплатный расчет</div>
                    <div class="order__text">
                        Строим по любым проектам.<br/>
                        Присылайте свой вариант нам на расчет.
                    </div>
                    <div class="order__button">
                        <a href="#calculation" class="btn btn-md btn-modal">Заказать расчет</a>
                    </div>
                </div>
            </div>
        </div><!--/.container -->
    </div><!--/.order -->

    <div class="root__main">
        <div class="container">
            <div class="root__main--content">
                <div class="about">
                    <div class="about__main">
                        <div class="about__main--header">Сео заголовок</div>
                        <div class="about__main--content">
                            <div class="collapse">
                                <div class="collapse-content">
                                    {$_modx->resource.content}
                                </div>
                                <button class="btn-link collapse-control" data-down="Развернуть" data-up="Свeрнуть"></button>
                            </div>
                        </div>
                    </div>
                    <div class="about__media">
                        <div class="about__media--image">
                            <img src="assets/template/images/content/about__image.webp" class="img-fluid" alt="">
                        </div>
                    </div>
                    <!--/.about__media--image -->
                </div>
                <!--/.about__media -->
            </div>
            <!--/.about -->
        </div>
        <!--/.container -->
    </div>

    {include 'file:templates/common/_visualization.tpl'}

    <div class="advantages">
        <div class="container">
            <div class="advantages__wrap">
                <div class="advantages__header">
                    <div class="advantages__header--num">6</div>
                    <div class="advantages__header--text">
                        наших<br/>
                        неоспоримых<br/>
                        преимуществ
                    </div>
                </div><!--/.advantages__wrap -->
                <div class="advantages__main">
                    <div class="advantages__item">
                        <div class="advantages__item--wrap">
                            <div class="advantages__item--icon">
                                <svg class="ico-svg" viewBox="0 0 459.249 459.249" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="assets/template/images/sprites/sprite-icons.svg#carpenter" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="advantages__item--text">Строим с 2009 года</div>
                        </div>
                    </div><!--/.advantages__item -->
                    <div class="advantages__item">
                        <div class="advantages__item--wrap">
                            <div class="advantages__item--icon">
                                <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="assets/template/images/sprites/sprite-icons.svg#cabin" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="advantages__item--text">
                                Строим по технологии в 2 этапа.<br/>
                                Под усадку и под ключ.
                            </div>
                        </div>
                    </div><!--/.advantages__item -->
                    <div class="advantages__item">
                        <div class="advantages__item--wrap">
                            <div class="advantages__item--icon">
                                <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="assets/template/images/sprites/sprite-icons.svg#price-tag" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="advantages__item--text">Цены ниже рыночных</div>
                        </div>
                    </div><!--/.advantages__item -->
                    <div class="advantages__item">
                        <div class="advantages__item--wrap">
                            <div class="advantages__item--icon">
                                <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="assets/template/images/sprites/sprite-icons.svg#purse" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="advantages__item--text">Работаем без предоплат</div>
                        </div>
                    </div><!--/.advantages__item -->
                    <div class="advantages__item">
                        <div class="advantages__item--wrap">
                            <div class="advantages__item--icon">
                                <svg class="ico-svg" viewBox="0 0 512.467 512.467" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="assets/template/images/sprites/sprite-icons.svg#forest" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="advantages__item--text">Собственное производство</div>
                        </div>
                    </div><!--/.advantages__item -->
                    <div class="advantages__item">
                        <div class="advantages__item--wrap">
                            <div class="advantages__item--icon">
                                <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="assets/template/images/sprites/sprite-icons.svg#russia" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="advantages__item--text">Построили свыше 1500 домов по все России</div>
                        </div>
                    </div><!--/.advantages__item -->
                </div><!--/.advantages__main -->
            </div>
        </div>
    </div><!--/.advantages -->

    <div class="videos">
        <div class="container">
            <div class="videos__header">Видеоотзывы</div><!--/.videos__header -->
            <div class="videos__main">
                <div class="videos__slider">
                    <div class="swiper-container videos-slider">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide videos--lg">
                                <a href="https://youtu.be/NxcHO-IWNvo" class="videos__item videos__item--lg data-fancybox">
                                    <div class="videos__item--image" style="background-image: url('assets/template/images/content/video__01.jpg')"></div>
                                    <div class="videos__item--text">Видеоотзыв от клиента который заказал дом из бруса</div>
                                    <div class="videos__item--button">
                                        <div class="videos__item--play">
                                            <img src="assets/template/images/icon__play.webp" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div><!--/.swiper-slide -->
                            <div class="swiper-slide videos--sm">
                                <a href="https://youtu.be/NxcHO-IWNvo" class="videos__item videos__item--sm data-fancybox">
                                    <div class="videos__item--image" style="background-image: url('assets/template/images/content/video__02.jpg')"></div>
                                    <div class="videos__item--text"></div>
                                    <div class="videos__item--button">
                                        <div class="videos__item--play">
                                            <img src="assets/template/images/icon__play.webp" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div><!--/.swiper-slide -->

                            <div class="swiper-slide videos--lg">
                                <a href="https://youtu.be/NxcHO-IWNvo" class="videos__item videos__item--lg data-fancybox">
                                    <div class="videos__item--image" style="background-image: url('assets/template/images/content/video__01.jpg')"></div>
                                    <div class="videos__item--text">Видеоотзыв от клиента который заказал дом из бруса</div>
                                    <div class="videos__item--button">
                                        <div class="videos__item--play">
                                            <img src="assets/template/images/icon__play.webp" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div><!--/.swiper-slide -->
                            <div class="swiper-slide videos--sm">
                                <a href="https://youtu.be/NxcHO-IWNvo" class="videos__item videos__item--sm data-fancybox">
                                    <div class="videos__item--image" style="background-image: url('assets/template/images/content/video__02.jpg')"></div>
                                    <div class="videos__item--text"></div>
                                    <div class="videos__item--button">
                                        <div class="videos__item--play">
                                            <img src="assets/template/images/icon__play.webp" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div><!--/.swiper-slide -->

                            <div class="swiper-slide videos--lg">
                                <a href="https://youtu.be/NxcHO-IWNvo" class="videos__item videos__item--lg data-fancybox">
                                    <div class="videos__item--image" style="background-image: url('assets/template/images/content/video__01.jpg')"></div>
                                    <div class="videos__item--text">Видеоотзыв от клиента который заказал дом из бруса</div>
                                    <div class="videos__item--button">
                                        <div class="videos__item--play">
                                            <img src="assets/template/images/icon__play.webp" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div><!--/.swiper-slide -->
                            <div class="swiper-slide videos--sm">
                                <a href="https://youtu.be/NxcHO-IWNvo" class="videos__item videos__item--sm data-fancybox">
                                    <div class="videos__item--image" style="background-image: url('assets/template/images/content/video__02.jpg')"></div>
                                    <div class="videos__item--text"></div>
                                    <div class="videos__item--button">
                                        <div class="videos__item--play">
                                            <img src="assets/template/images/icon__play.webp" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div><!--/.swiper-slide -->

                            <div class="swiper-slide videos--lg">
                                <a href="https://youtu.be/NxcHO-IWNvo" class="videos__item videos__item--lg data-fancybox">
                                    <div class="videos__item--image" style="background-image: url('assets/template/images/content/video__01.jpg')"></div>
                                    <div class="videos__item--text">Видеоотзыв от клиента который заказал дом из бруса</div>
                                    <div class="videos__item--button">
                                        <div class="videos__item--play">
                                            <img src="assets/template/images/icon__play.webp" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div><!--/.swiper-slide -->
                            <div class="swiper-slide videos--sm">
                                <a href="https://youtu.be/NxcHO-IWNvo" class="videos__item videos__item--sm data-fancybox">
                                    <div class="videos__item--image" style="background-image: url('assets/template/images/content/video__02.jpg')"></div>
                                    <div class="videos__item--text"></div>
                                    <div class="videos__item--button">
                                        <div class="videos__item--play">
                                            <img src="assets/template/images/icon__play.webp" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div><!--/.swiper-slide -->
                        </div>
                    </div>
                </div><!--/.videos__slider -->
            </div><!--/.videos__main -->
        </div>
    </div><!--/.videos -->

    {include 'file:templates/common/_how-work.tpl'}

    {include 'file:templates/common/_portfolio.tpl'}

    {include 'file:templates/common/_geography.tpl'}

{/block}



