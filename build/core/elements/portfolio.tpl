{extends 'file:templates/common/_layout.tpl'}

{block 'main'}

    <section class="root__main main">
        <div class="container">
            <div class="root__main--content">
                {$_modx->resource.content}
                <div class="row">
                    {set $rows = json_decode($_modx->resource.portfolio_gallery, true)}
                    {foreach $rows as $row}
                        {var $img = 'phpThumbOn' | snippet : ['input' => $row.image,  'options' => 'w=680&h=506&zc=1']}
                        <div class="col-xs-12 col-md-6 col-xl-4 mb-30">
                            <div class="portfolio-item">
                                <a href="{$row.image}" class="portfolio-item__image" data-fancybox="gallery">
                                    <img src="{$img}" class="img-fluid" alt="">
                                </a>
                            </div><!--/.portfolio-item -->
                        </div>
                    {/foreach}
                </div><!--/.portfolio__content -->
            </div>
        </div>
    </section>

{/block}



