const srcPath = 'src';
const destPath = 'build';

const config = {
    src: {
        root: srcPath,
        sass: `${srcPath}/scss`,
        js: `${srcPath}/js`,
        fonts: `${srcPath}/assets/fonts`,
        images: `${srcPath}/assets/images`,
        icons: `${srcPath}/assets/icons`,
        html: `${srcPath}/html`,
        tpl: `${srcPath}/tpl`
    },

    dest: {
        root: destPath,
        html: destPath,
        css: `${destPath}/assets/template/css`,
        js: `${destPath}/assets/template/js`,
        fonts: `${destPath}/assets/template/fonts`,
        images: `${destPath}/assets/template/images`,
        tpl: `${destPath}/core/elements`,
    },

    setEnv() {
        this.isProd = process.argv.includes('--prod');
        this.isDev = !this.isProd;
        },
};

export default config;
