import gulp from 'gulp';
import config from '../config';

const fontsBuild = () => (
    gulp.src(`${config.src.fonts}/**/*`)
    .pipe(gulp.dest(config.dest.fonts))
);

export const assetsBuild = gulp.parallel(fontsBuild);

export const assetsWatch = () => {
	gulp.watch(`${config.src.fonts}/**/*`, fontsBuild);
};


const tplBuild = () => (
    gulp.src(`${config.src.tpl}/**/*`)
        .pipe(gulp.dest(config.dest.tpl))
);
export const modxBuild = gulp.parallel(tplBuild);

export const modxWatch = () => {
    gulp.watch(`${config.src.tpl}/**/*`, tplBuild);
};
