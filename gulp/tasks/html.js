import gulp from 'gulp';
import gulpif from 'gulp-if';
import plumber from 'gulp-plumber';
import formatHtml from 'gulp-format-html';
import nunjucksRender from 'gulp-nunjucks-render';
import nunjucksIncludeData from 'nunjucks-includeData';
import { setup as emittySetup } from '@zoxon/emitty';
import config from '../config';

const emittyNunjucks= emittySetup(config.src.html, 'nunjucks', {
    makeVinylFile: true,
});

global.isNunjucksWatch = false;
global.emittyChangedFile = {
    path: '',
    stats: null,
};


export const htmlBuild = (done) => {
    gulp.src(`${config.src.html}/*.njk`)
        .pipe(plumber())
        .pipe(
            gulpif(
                global.isNunjucksWatch,
                emittyNunjucks.stream(
                    global.emittyChangedFile.path,
                    global.emittyChangedFile.stats,
                ),
            ),
        )
        .pipe(nunjucksRender())
        .pipe(formatHtml())
        .pipe(gulp.dest(config.dest.html))
    done();
};

export const htmlWatch = () => {
    global.isNunjucksWatch = true;

    gulp.watch(`${config.src.html}/**/*.njk`, htmlBuild)
        .on('all', (event, filepath, stats) => {
            global.emittyChangedFile = {
                path: filepath,
                stats,
            };
        });
};
