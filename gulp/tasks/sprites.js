import gulp from 'gulp';
import svgSprite from 'gulp-svg-sprite';
import config from '../config';

const sprite = () => (
    gulp.src(`${config.src.icons}/**/*.svg`)
        .pipe(svgSprite({
            mode: {
                symbol: {
                    sprite: '../sprites/sprite-icons.svg',
                },
            },
            shape: {
                transform: [
                    {
                        svgo: {
                            js2svg: {pretty: true},
                            plugins: [
                                {
                                    removeAttrs: {
                                        attrs: ['class', 'data-name', 'fill.*', 'stroke.*'],
                                    },
                                },
                            ],
                        },
                    },
                ],
            },
        }))
        .pipe(gulp.dest(config.dest.images))
);

export const spritesBuild = gulp.parallel(sprite);

export const spritesWatch = () => {
    gulp.watch(`${config.src.icons}/**/*.svg`, sprite);
};
