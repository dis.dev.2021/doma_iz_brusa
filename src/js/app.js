import './plugins/import-jquery';
import 'focus-visible';
import lazyImages from './plugins/lazyImages';
import documentReady from './helpers/documentReady';
import mobileNav from './components/mobile-nav';
import collapce from './components/collapse';
import maskPhone from './plugins/phone-mask';
import tabs from './components/tabs';
import accordion from './components/accordion';
import Choices from 'choices.js';
import '@fancyapps/fancybox'
import {WebpMachine} from "webp-hero"
import Swiper, {Autoplay, Navigation, Pagination, Thumbs} from 'swiper';
Swiper.use([Navigation, Pagination, Autoplay, Thumbs]);
import textInputBoxPlugin from './plugins/text-input-box';

documentReady(() => {
    lazyImages();
    tabs();
    mobileNav();
    collapce();
    accordion();
    maskPhone('input[name="phone"]');

    // IE Webp Support
    const webpMachine = new WebpMachine()
    webpMachine.polyfillDocument()

    // Select
    const selectElem = document.querySelector('.pretty-select');
    if (selectElem) {
        const prettySelect = new Choices(selectElem);
    }

    // Input + placeholder
    const $body = $('body');
    textInputBoxPlugin($);
    $body.textInputBox();

    // Modal
    $('.btn-modal').fancybox({
        autoFocus: false,
    });

    // Catalog
    $('.calc__group input[type="radio"]').on('change', function(e) {
        e.preventDefault();
        let calcBox = $($(this).closest('.calc__group'));
        let isCalcOpen = calcBox.hasClass('complete');
        if (!isCalcOpen) {
            calcBox.addClass('complete');
            let stepNum = calcBox.attr('data-next');
            let calcBoxClass = '#calc-group-' + stepNum;
            $(calcBoxClass).addClass('open');
            console.log(stepNum);
        }
    });

    $('.calc__group--address').on("focus", function (e) {
        e.preventDefault();
        $('.calc__summary').toggleClass('open');
    });

    // Project Gallery
    const isProject = document.querySelector('.project');
    if (isProject) {
        const projectThumbs = new Swiper('.project-thumbs', {
            loop: true,
            spaceBetween: 10,
            slidesPerView: 3,
            freeMode: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            navigation: {
                nextEl: ".project-thumbs-next",
                prevEl: ".project-thumbs-prev",
            },
            breakpoints: {
                768: {
                    slidesPerView: 4,
                },
                992: {
                    slidesPerView: 3,
                },
            },
        });

        const projectGallery = new Swiper('.project-gallery', {
            loop: true,
            spaceBetween: 30,
            thumbs: {
                swiper: projectThumbs,
            },
        });
    }

    // Project Options

    $('.project-options__item').on('click', function (e){
        e.preventDefault();
        let priceBox = $('.project-price');
        let optionsItem = $(this);
        let optionPrice = optionsItem.attr('data-price');
        let optionsGroup = optionsItem.closest('.project-options__group');
        let nullString = 'по запросу';

        optionsGroup.find('.project-options__item').removeClass('active');
        optionsItem.addClass('active');

        if (optionPrice != 'по запросу') {

        }

        if (optionsGroup.hasClass('project-options__group--one')) {
            priceBox.attr('data-walls', optionPrice);
        }

        if (optionsGroup.hasClass('project-options__group--two')) {
            priceBox.attr('data-foundation', optionPrice);
        }

        if (optionsGroup.hasClass('project-options__group--three')) {
            priceBox.attr('data-roof', optionPrice);
        }

        let optionWalls = priceBox.attr('data-walls');
        let optionFoundation = priceBox.attr('data-foundation');
        let optionRoof = priceBox.attr('data-roof');
        let priceTotal

        if ((optionWalls != nullString) && (optionFoundation != nullString) && (optionRoof != nullString) ) {
            priceTotal = parseInt(optionWalls) + parseInt(optionFoundation) + parseInt(optionRoof);
            $('.project-summary').text(priceTotal.toLocaleString('ru-RU'));
            $('.modal-price').text(priceTotal.toLocaleString('ru-RU'));
        }


    })

    // Portfolio
    const portfolioGallery = new Swiper('.portfolio-slider', {
        loop: true,
        slidesPerView: 'auto',
        spaceBetween: 15,
        navigation: {
            nextEl: ".portfolio-nav-next",
            prevEl: ".portfolio-nav-prev",
        },
        breakpoints: {
            768: {
                spaceBetween: 30,
            }
        },
    });

    // Primary screen
    const isPrimary = document.querySelector('.primary');
    if (isPrimary) {
        const primarySlider = new Swiper('.primary-slider', {
            loop: true,
            spaceBetween: 30,
            navigation: {
                nextEl: ".primary-nav-next",
                prevEl: ".primary-nav-prev",
            },
        });

        primarySlider.on('slideChange', function () {
            let slideNum = primarySlider.realIndex + 1;
            slideNum = "0" + slideNum;
            console.log(slideNum);
            let slideSwitch = '.slide-item-' + primarySlider.realIndex;
            $('.primary__switch--item').removeClass('active');
            $(slideSwitch).addClass('active');
            $('.primary-counter').text(slideNum);
        });

        $('.primary__switch--item').on('click', function (e){
            e.preventDefault();
            let item = $(this);
            let slideNum = item.attr('data-slide');
            $('.primary__switch--item').removeClass('active');
            item.addClass('active');
            primarySlider.slideTo(slideNum, 600)
        })
    }

    // Videos slider
    const isVideos = document.querySelector('.videos');
    if (isVideos) {
        const videoSlider = new Swiper('.videos-slider', {
            loop: false,
            slidesPerView: 'auto',
            spaceBetween: 20,
            navigation: {
                nextEl: ".videos-nav-next",
                prevEl: ".videos-nav-prev",
            },
            breakpoints: {
                768: {
                    spaceBetween: 30,
                }
            },
        });
    }

    // input file
    $('.file-form input[type="file"]').on('change', function(e) {
        let fileField = $(this).closest('.file-form');
        let str = $(this).val();
        let strName;
        console.log(str);
        if (str.lastIndexOf('\\')){
            strName = str.lastIndexOf('\\')+1;
        }
        else{
            strName = str.lastIndexOf('/')+1;
        }
        let filename = str.slice(strName);

        if (filename) {
            fileField.find('.file-form__label span').text(filename);
        }
        else {
            fileField.find('.file-form__label span').text(fileField.find('.file-form__label').attr('data-placeholder'));
        }
    });

});


