{extends 'file:templates/common/_layout.tpl'}

{block 'heading'}
{/block}

{block 'main'}
<div class="root__main error-page">
    <div class="error-page__wrap">
        <div class="container">
            <div class="error-page__top">О опс!</div>
            <div class="error-page__header">4<span>0</span>4</div>
            <div class="error-page__title">Страница не найдена</div>
            <div class="error-page__button">
                <a href="/" class="btn">На главную</a>
            </div>
        </div>
    </div>
</div>
{/block}

