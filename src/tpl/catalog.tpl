{extends 'file:templates/common/_layout.tpl'}


{block 'heading'}
    <div class="root__heading heading-catalog">
        <div class="heading-catalog__slide" style="background-image: url('assets/template/images/heading__bg.webp')">
            <div class="heading-catalog__main">
                <div class="container">
                    <div class="heading-catalog__title">{$_modx->resource.pagetitle}</div>
                    <div class="heading-catalog__text">{$_modx->resource.introtext}</div>
                </div>
            </div>
        </div><!--/.heading-catalog__slide -->
        <div class="heading-catalog__switch">
            <div class="heading-catalog__switch--container">
                <a href="{48 | url}" class="heading-catalog__switch--item slide-item-0" data-slide="1">
                    <strong>01.</strong>
                    <span>
                        Дома из бруса<br/>
                        от 310 000 руб.
                    </span>
                </a>
                <a href="{49 | url}" class="heading-catalog__switch--item slide-item-1" data-slide="2">
                    <strong>02.</strong>
                    <span>
                        Бани из бруса<br/>
                        от 270 000 руб.
                    </span>
                </a>
            </div>
        </div>
        <div class="heading-catalog__pattern">
            <div class="container header__container">
                <div class="heading-catalog__pattern--wrap">
                    <div class="heading-catalog__pattern--lines"></div>
                </div>
            </div>
        </div>
    </div><!--/.heading-catalog -->
{/block}

{block 'main'}


    {'!mFilter2' | snippet : [
    'parents' => $_modx->resource['id'],
    'limit' => '14',
    'paginator' => 'pdoPage',
    'ajaxMode' => 'default',
    'includeThumbs' => 'small',
    'class' => 'msProduct',
    'element' => 'msProducts',
    'tpl' => 'tpl.goods',
    'tplOuter' => 'tpl.filter.outer',
    'filters' => '
            msoption|cf_type,
            msoption|cf_size,
            msoption|cf_floors,
            msoption|cf_roof,
            msoption|cf_size_params,
            msoption|cf_room,
            msoption|cf_bedroom,
            msoption|cf_construct,
            msoption|cf_season,
            ms|price:number,
            msoption|cf_price_group,

                    ',
'aliases' => '
            msoption|cf_type==type,
            msoption|cf_size==size,
            msoption|cf_floors==floors,
            msoption|cf_roof==roof,
            msoption|cf_size_params==size_params,
            msoption|cf_room==room,
            msoption|cf_bedroom==bedroom,
            msoption|cf_construct==construct,
            msoption|cf_season==season,
            msoption|cf_price_group==price_group,
        ',

'tplFilter.outer.type' => 'tpl.filter.group.01',
'tplFilter.row.type' => 'tpl.filter.checkbox',

'tplFilter.outer.size' => 'tpl.filter.group.02',
'tplFilter.row.size' => 'tpl.filter.checkbox',

'tplFilter.outer.floors' => 'tpl.filter.group.03',
'tplFilter.row.floors' => 'tpl.filter.checkbox',

'tplFilter.outer.roof' => 'tpl.filter.group.04',
'tplFilter.row.roof' => 'tpl.filter.checkbox',

'tplFilter.outer.size_params' => 'tpl.filter.group.05',
'tplFilter.row.size_params' => 'tpl.filter.checkbox',

'tplFilter.outer.room' => 'tpl.filter.group.06',
'tplFilter.row.room' => 'tpl.filter.checkbox',

'tplFilter.outer.bedroom' => 'tpl.filter.group.07',
'tplFilter.row.bedroom' => 'tpl.filter.checkbox',

'tplFilter.outer.construct' => 'tpl.filter.group.08',
'tplFilter.row.construct' => 'tpl.filter.checkbox',

'tplFilter.outer.season' => 'tpl.filter.group.09',
'tplFilter.row.season' => 'tpl.filter.checkbox',

'tplFilter.outer.ms|price' => 'tpl.filter.group.10',
'tplFilter.row.ms|price' => 'tpl.filter.number',

'tplFilter.outer.price_group' => 'tpl.filter.group.11',
'tplFilter.row.price_group' => 'tpl.filter.button',
]}

    <div class="calc" id="calc">
        <div class="container">
            <div class="calc__header">Калькулятор расчета стоимости дома</div>
            <div class="calc__main">
                {include 'calc'}
            </div>
        </div>
    </div>

    <div class="root__main">
        <div class="container">
            <div class="root__main--content">
                <div class="about">
                    <div class="about__main">
                        <div class="about__main--header">Сео заголовок</div>
                        <div class="about__main--content">
                            <div class="collapse">
                                <div class="collapse-content">
                                    {$_modx->resource.content}
                                </div>
                                <button class="btn-link collapse-control" data-down="Развернуть" data-up="Свeрнуть"></button>
                            </div>
                        </div>
                    </div>
                    <div class="about__media">
                        <div class="about__media--image">
                            <img src="assets/template/images/content/about__image.webp" class="img-fluid" alt="">
                        </div>
                    </div><!--/.about__media--image -->
                </div><!--/.about__media -->
            </div><!--/.about -->
        </div><!--/.container -->
    </div><!--/.root__main -->

{/block}

