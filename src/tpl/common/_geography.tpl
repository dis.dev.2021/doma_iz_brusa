<div class="geography">
    <div class="container">
        <div class="geography__row">
            <div class="geography__media">
                <div class="geography__media--map">
                    <img src="assets/template/images/geography__map.webp" class="img-fluid" alt="">
                </div>
            </div>
            <div class="geography__content">
                <div class="geography__header">География строительства</div>
                <div class="geography__lead">Мы построили более 650 домов по Москве, Московской области и другим регионам. Лучших отзыв – это реальное общение с реальным клиентом и осмотр построенного объекта вживую</div>
                <div class="geography__text">Заказать строительство бани можно в Московской, Ленинградской, Нижегородской, Новгородской, Ярославской, Калужской, Ивановской, Костромской, Псковской, Владимирской, Вологодской, Тверской, Тульской, Рязанской областях и также во многих городах: Москве, Подмосковье, СПБ, Великом Новгороде, Ярославле, Калуге, Вологде, Пестово, Владимире, Костроме, Иваново, Туле, Рязани и других городах России</div>
                <div class="geography__button">
                    <a href="{13 | url}" class="btn">Смотреть на реальные объекты</a>
                </div>
            </div>
        </div>
    </div>
</div>
