{set $id = $_modx->resource.id}
{set $title = ($_modx->resource.longtitle ?: $_modx->resource.pagetitle) | notags}
{set $description = $_modx->resource.description | replace :' "':' «' | replace :'"':'»'}
{set $page = 'site_url' | config ~ $_modx->resource.uri}


<!DOCTYPE html>
<html lang="ru">

<head>

    {block 'head'}
        <base href="{'site_url' | config}" />
        <meta charset="utf-8">
        <title>{$title} | {'site_name' | config}</title>
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="{$description}">
        <meta name="keywords" content="{'seoPro.keywords' | placeholder}">
        <meta name="robots" content="{'seoTab.robotsTag' | placeholder}">

        <meta name="csrf-token" content="{$session['csrf-token']}">

        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />


        <!-- Facebook Open Graph -->
        <meta property="og:url" content="{$page}">
        <meta property="og:type" content="website">
        <meta property="og:title" content="{$title}">
        <meta property="og:image" content="">
        <meta property="og:description" content="{$description}">
        <meta property="og:site_name" content="{'site_name' | config}">

        <!-- Schema.org -->
        <meta itemprop="name" content="{$title}">
        <meta itemprop="description" content="{$description}">
        <meta itemprop="image" content="">

        <!-- dns-prefetch -->
        <link href='//fonts.googleapis.com' rel='dns-prefetch'>
        <link href='//ajax.googleapis.com' rel='dns-prefetch'>
        <link href='//cdn.jsdelivr.net' rel='dns-prefetch'>
        <link href='//cdn.polyfill.io' rel='dns-prefetch'>
        <link href='//cdnjs.cloudflare.com' rel='dns-prefetch'>
        <link href='//unpkg.com/' rel='dns-prefetch'>

        <!-- preconnect -->
        <link href="https://fonts.gstatic.com" rel="preconnect" crossorigin>

        <link rel="stylesheet" href="assets/template/css/main.min.css">
    {/block}
</head>

<body>

<div class="root">

    {block 'header'}
        <header class="root__header header">
            <div class="container header__container">
                <div class="header__wrap">
                    <div class="header__hamburger">
                        <button type="button" class="hamburger nav-toggle">
                        <span class="hamburger__icon">
                            <span class="hamburger__icon--inner"></span>
                        </span>
                            <span class="hamburger__text">Меню</span>
                        </button><!--/.hamburger -->
                    </div><!--/.header__hamburger -->
                    <a href="/" class="header__logo">
                        <span class="header__logo--wrap">LOGO</span>
                    </a><!--/.header__logo -->
                    <div class="header__info">
                        <div class="header__info--item">
                            <div class="header__info--label">Почта</div>
                            <div class="header__info--value">
                                <a href="mailto:brus-doma@yandex.ru">{'email_primary' | config}</a>
                            </div>
                        </div><!--/.header__info--item -->
                        <div class="header__info--item">
                            <div class="header__info--label">Рабочие часы</div>
                            <div class="header__info--value">{'worktime' | config}</div>
                        </div><!--/.header__info--item -->
                    </div><!--/.header__info -->
                    <a href="#calculation" class="header__order btn-modal">
                        <i class="header__order--icon">
                            <svg class="ico-svg" viewBox="0 0 512 512"  xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="assets/template/images/sprites/sprite-icons.svg#calculator" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <span class="header__order--text">Отправить проект на расчет</span>
                    </a><!--/.header__order -->
                    <div class="header__contact">
                        <div class="header__contact--phone">
                            <a href="tel:{'phone_primary' | config | preg_replace : '/[^0-9+]/' : ''}" class="header__phone">
                                <i class="header__phone--icon header__phone--viber">
                                    <svg class="ico-svg" viewBox="0 0 24 24"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#viber" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                                <i class="header__phone--icon header__phone--whatsapp">
                                    <svg class="ico-svg" viewBox="0 0 682 682.667"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#whatsapp" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                                <div class="header__phone--number">{'phone_primary' | config}</div>
                            </a>
                        </div>
                        <div class="header__contact--text">звонок по России бесплатный</div>
                    </div><!--/.header__contact -->
                </div><!--/.header__row -->
            </div><!--/.container -->
        </header><!--/.header -->
    {/block}

    {block 'nav'}
        <nav class="nav">
            <div class="nav__close nav-toggle"></div><!--/.nav__close -->
            {'pdoMenu' | snippet : [
            'startId' => 0,
            'level' => 2,
            'resources' => '-1',
            'outerClass' => 'nav__menu',
            'firstClass' => 'nav-first',
            'lastClass' => 'nav-last',
            ]}<!--/.nav__menu -->
        </nav><!--/.nav -->
        <div class="nav__layout nav-toggle"></div><!--/.nav__layout -->
    {/block}

    {block 'heading'}
        <div class="root__heading heading" style="background-image: url('assets/template/images/heading__bg.webp')">
            <div class="container">
                <div class="heading__container">
                    <div class="heading__wrap">
                        <h1 class="heading__title">{$_modx->resource.pagetitle}</h1>
                    </div>
                </div>
            </div><!--/.container -->
        </div><!--/.heading -->
    {/block}

    {block 'main'}
        <section class="root__main main">
            <div class="container">
                <div class="root__main--content">
                    {$_modx->resource.content}
                </div>
            </div>
        </section>
    {/block}

    {block 'bottom'}
    {/block}

    {block 'footer'}
        <footer class="root__footer footer">
            <div class="container">
                <div class="footer__main">
                    <div class="footer__content">
                        <a href="/" class="footer__logo">LOGO</a><!--/.footer__logo -->
                        <div class="footer__text">Сайт носит исключительно информационный характер и ни при каких условиях не является публичной офертой. Для получения подробной информации о стоимости обращайтесь по телефонам.</div>
                    </div><!--/.footer__content -->
                    <div class="footer__nav">
                        <div class="footer__title">Меню</div><!--/.footer__title -->
                        {'pdoMenu' | snippet : [
                        'startId' => 0,
                        'level' => 1,
                        'outerClass' => 'footer__menu'
                        ]}<!--/.footer__menu -->
                    </div><!--/.footer__nav -->
                    <div class="footer__popular">
                        <div class="footer__title">Популярные размеры</div><!--/.footer__title -->
                        <div class="footer__popular--row">
                            <div class="footer__popular--col">
                                <ul class="footer__menu">
                                    <li>
                                        <a href="{'site_url' | config}catalog/6x4">6х4</a>
                                    </li>
                                    <li>
                                        <a href="{'site_url' | config}catalog/6x5">6х5</a>
                                    </li>
                                    <li>
                                        <a href="{'site_url' | config}catalog/6x6">6х6</a>
                                    </li>
                                    <li>
                                        <a href="{'site_url' | config}catalog/6x7">6х7</a>
                                    </li>
                                </ul><!--/.footer__menu -->
                            </div><!--/.footer__popular--col -->
                            <div class="footer__popular--col">
                                <ul class="footer__menu">
                                    <li>
                                        <a href="{'site_url' | config}catalog/6x8">6х8</a>
                                    </li>
                                    <li>
                                        <a href="{'site_url' | config}catalog/6x9">6х9</a>
                                    </li>
                                    <li>
                                        <a href="{'site_url' | config}catalog/7x7">7х7</a>
                                    </li>
                                    <li>
                                        <a href="{'site_url' | config}catalog/7x8">7х8</a>
                                    </li>
                                </ul><!--/.footer__menu -->
                            </div><!--/.footer__popular--col -->
                            <div class="footer__popular--col">
                                <ul class="footer__menu">
                                    <li>
                                        <a href="{'site_url' | config}catalog/7x9">7х9</a>
                                    </li>
                                    <li>
                                        <a href="{'site_url' | config}catalog/8x8">8х8</a>
                                    </li>
                                    <li>
                                        <a href="{'site_url' | config}catalog/8x9">8х9</a>
                                    </li>
                                    <li>
                                        <a href="{'site_url' | config}catalog/8x10">8х10</a>
                                    </li>
                                </ul><!--/.footer__menu -->
                            </div><!--/.footer__popular--col -->
                            <div class="footer__popular--col">
                                <ul class="footer__menu">
                                    <li>
                                        <a href="{'site_url' | config}catalog/9x9">9х9</a>
                                    </li>
                                    <li>
                                        <a href="{'site_url' | config}catalog/9x10">9х10</a>
                                    </li>
                                    <li>
                                        <a href="{'site_url' | config}catalog/10x10">10х10</a>
                                    </li>
                                    <li>
                                        <a href="{'site_url' | config}catalog/10x12">10х12</a>
                                    </li>
                                </ul><!--/.footer__menu -->
                            </div><!--/.footer__popular--col -->
                        </div><!--/.footer__popular--row -->
                    </div><!--/.footer__popular -->
                    <div class="footer__project">
                        <div class="footer__title">Проекты</div><!--/.footer__title -->
                        {'pdoMenu' | snippet : [
                        'startId' => 11,
                        'level' => 1,
                        'outerClass' => 'footer__menu'
                        ]}<!--/.footer__menu -->
                    </div><!--/.footer__project -->
                    <div class="footer__contact">
                        <div class="footer__title">Контакты</div><!--/.footer__title -->
                        <ul class="footer__contact--list">
                            <li>{'address' | config}</li>
                            <li>
                                <div>
                                    <a href="tel:{'phone_primary' | config | preg_replace : '/[^0-9+]/' : ''}" data-type="phone">{'phone_primary' | config}</a>
                                </div>
                                <div>
                                    <a href="tel:{'phone_second' | config | preg_replace : '/[^0-9+]/' : ''}" data-type="phone">{'phone_second' | config}</a>
                                </div>
                            </li>
                            <li>
                                <a href="mailto:{'email_primary' | config}" class="">{'email_primary' | config}</a>
                            </li>
                            <li>{'worktime' | config} без выходных</li>
                        </ul><!--/.footer__contact--list -->
                        <div class="footer__contact--social">
                            <div class="footer__social">
                                <a href="{'link_odnoklassniki' | config}" class="footer__social--item">
                                    <i>
                                        <svg class="ico-svg" viewBox="0 0 320 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#odnoklassniki" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </a><!--/.footer__social--item -->
                                <a href="{'link_vkontakte' | config}" class="footer__social--item">
                                    <i>
                                        <svg class="ico-svg" viewBox="0 0 576 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#vk" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </a><!--/.footer__social--item -->
                                <a href="{'link_facebook' | config}" class="footer__social--item">
                                    <i>
                                        <svg class="ico-svg" viewBox="0 0 264 512"  xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#facebook-f" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </i>
                                </a><!--/.footer__social--item -->
                            </div><!--/.footer__social -->
                        </div><!--/.footer__contact--social -->
                    </div><!--/.footer__contact -->
                    <div class="footer__sm">
                        <div class="footer__social">
                            <a href="{'link_odnoklassniki' | config}" class="footer__social--item">
                                <i>
                                    <svg class="ico-svg" viewBox="0 0 320 512"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#odnoklassniki" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a><!--/.footer__social--item -->
                            <a href="{'link_vkontakte' | config}" class="footer__social--item">
                                <i>
                                    <svg class="ico-svg" viewBox="0 0 576 512"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#vk" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a><!--/.footer__social--item -->
                            <a href="{'link_facebook' | config}" class="footer__social--item">
                                <i>
                                    <svg class="ico-svg" viewBox="0 0 264 512"  xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#facebook-f" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a><!--/.footer__social--item -->
                        </div><!--/.footer__social -->
                    </div>
                </div><!--/.footer__main -->

                <div class="footer__bottom">
                    <div class="footer__bottom--item">
                        <div class="footer__copy">© 2009-2021 Строительство каркасных домов. Все права защищены.</div>
                    </div><!--/.footer__bottom--item -->
                    <div class="footer__bottom--item">
                        <a href="{8 | url}" class="footer__bottom--privacy">Политика конфиденциальности</a>
                    </div><!--/.footer__bottom--item -->
                    <div class="footer__bottom--item">
                        <div class="footer__bottom--dev">
                            <span>Разработка сайта:</span>
                            <a href="#">Creative.com</a>
                        </div>
                    </div><!--/.footer__bottom--item -->
                </div><!--/.footer__bottom -->

            </div><!--/.container -->
        </footer><!--/.footer -->

    {/block}

    {block 'modal'}
        <div class="hide">
            <!-- Callback -->
            <div class="popup" id="callback">
                <div class="popup__header">
                    <div class="popup__header--title">Оставьте заявку</div>
                    <div class="popup__header--text">и мы вам перезвоним!</div>
                </div>
                <div class="popup__content">
                    {include 'form.callback'}
                </div>
            </div><!--/.callback -->


            <!-- Modal Consultation -->
            <div class="modal modal-large" id="consultation">
                <div class="modal__header">Получите консультацию</div>
                <div class="modal__main">
                    {include 'form.promo'}
                </div>
            </div>
            <!-- -->

            <!-- Modal Calculation -->
            <div class="modal modal-large" id="calculation">
                <div class="modal__header">Рассчитать стоимость проекта</div>
                <div class="modal__main">
                    {include 'form.calculation'}
                </div>
            </div>
            <!-- -->

            <!-- Modal Order -->
            <div class="modal modal-large" id="order">
                <div class="modal__header mb-0">Оформление заявки <br class="hide-xs-only"/>строительство</div>
                <div class="modal__info">
                    <div class="modal__info--item">Выбран проект: <span class="color-green">{$_modx->resource.pagetitle}</span></div>
                    <div class="modal__info--item">Цена: <span class="color-green modal-price">{$_modx->resource.price}</span></div>
                </div>
                <div class="modal__main">
                    {include 'form.order'}
                </div>
            </div>
            <!-- -->

            <!-- Успешная отправка -->
            <div class="success" id="success">
                <div class="success__image">
                    <div class="success__image--icon">
                        <svg class="ico-svg" viewBox="0 0 510 510"  xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#tick-inside-circle" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </div>
                </div>
                <div class="success__title">Спасибо!</div>
                <div class="success__text">Ваша заявка отправлена</div>
            </div>
            <!-- -->
        </div>
    {/block}

</div>

{block 'scripts'}
    <script src="assets/template/js/app.js"></script>
{/block}

</body>

</html>




