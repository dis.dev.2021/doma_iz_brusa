<div class="visualization visualization--start">
    <div class="visualization__wrap">
        <div class="container">
            <div class="visualization__row">
                <div class="visualization__content">
                    <div class="visualization__header">Бесплатная  визуализация</div>
                    <div class="visualization__button visualization__button--md">
                        <a href="#callback" class="btn btn-modal">Хочу</a>
                    </div>
                </div>
                <div class="visualization__media">
                    <div class="visualization__media--image">
                        <img src="assets/template/images/visualization__image_01.webp" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="visualization__button visualization__button--sm">
                    <a href="#callback" class="btn btn-modal">Хочу</a>
                </div>
            </div><!-- /.visualization__row -->
        </div><!--/.container -->
    </div><!-- /.visualization__wrap -->
</div><!-- /.visualization -->

<div class="visualization visualization--finish">
    <div class="visualization__wrap">
        <div class="container">
            <div class="visualization__row">
                <div class="visualization__content"></div>
                <div class="visualization__media">
                    <div class="visualization__media--image">
                        <img src="assets/template/images/visualization__image_02.webp" class="img-fluid" alt="">
                    </div>
                </div>
            </div><!-- /.visualization__row -->
        </div><!--/.container -->
    </div><!-- /.visualization__wrap -->
</div><!-- /.visualization -->
