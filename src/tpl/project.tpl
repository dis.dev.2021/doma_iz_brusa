{extends 'file:templates/common/_layout.tpl'}

{block 'heading'}

    {set $price_delta = $_modx->resource.old_price - $_modx->resource.price}

    <div class="project">
        <div class="container">
            <div class="project__wrap">

                <div class="project__sm">
                    {'!pdoNeighbors' | snippet : [
                        'tplWrapper' => '@INLINE <div class="project__nav project-nav">[[+prev]][[+next]]</div>',
                        'tplPrev' => '@INLINE <a href="/[[+uri]]" class="project-nav__item project-nav__item--prev"><span class="project-nav__text">Предыдущий <br/>проект</span><i class="project-nav__arrow"><svg class="ico-svg" viewBox="0 0 476.213 476.213"  xmlns="http://www.w3.org/2000/svg"><use xlink:href="assets/template/images/sprites/sprite-icons.svg#right-arrow" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg></i></a><!--/.project-nav__item -->',
                        'tplNext' => '@INLINE <a href="/[[+uri]]" class="project-nav__item project-nav__item--next"><i class="project-nav__arrow"><svg class="ico-svg" viewBox="0 0 476.213 476.213"  xmlns="http://www.w3.org/2000/svg"><use xlink:href="assets/template/images/sprites/sprite-icons.svg#right-arrow" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg></i><span class="project-nav__text">Следующий <br/>проект</span></a><!--/.project-nav__item -->'
                    ]}
                    <h1 class="project__header">{$_modx->resource.pagetitle}</h1><!--/.project__header -->
                </div><!--/.project__sm -->

                <div class="project__media project-media">
                    {'!msGallery' | snippet : [
                        'tpl' => 'tpl.productGallery'
                    ]}
                </div><!--/.project-media -->

                <div class="project__main">
                    <div class="project__lg">
                        {'!pdoNeighbors' | snippet : [
                            'tplWrapper' => '@INLINE <div class="project__nav project-nav">[[+prev]][[+next]]</div>',
                            'tplPrev' => '@INLINE <a href="/[[+uri]]" class="project-nav__item project-nav__item--prev"><span class="project-nav__text">Предыдущий <br/>проект</span><i class="project-nav__arrow"><svg class="ico-svg" viewBox="0 0 476.213 476.213"  xmlns="http://www.w3.org/2000/svg"><use xlink:href="assets/template/images/sprites/sprite-icons.svg#right-arrow" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg></i></a><!--/.project-nav__item -->',
                            'tplNext' => '@INLINE <a href="/[[+uri]]" class="project-nav__item project-nav__item--next"><i class="project-nav__arrow"><svg class="ico-svg" viewBox="0 0 476.213 476.213"  xmlns="http://www.w3.org/2000/svg"><use xlink:href="assets/template/images/sprites/sprite-icons.svg#right-arrow" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg></i><span class="project-nav__text">Следующий <br/>проект</span></a><!--/.project-nav__item -->'
                        ]}
                        <h1 class="project__header">{$_modx->resource.pagetitle}</h1><!--/.project__header -->
                    </div><!--/.project__md -->
                    <div class="project__row">
                        <div class="project__cost">
                            <div class="project__cost--price project__price">
                                {if $_modx->resource.old_price}<div class="project__price--old">{$_modx->resource.old_price} руб</div>{else}{/if}
                                <div class="project__price--new">{if $_modx->resource.price}{$_modx->resource.price} р.{else}по запросу{/if}</div>
                            </div><!--/.project__price -->
                            <div class="project__cost--text">*Цена с доставкой и сборкой. Под усадку, без отделки, без окон и дверей, согласно комплектации</div>
                        </div><!--/.project__cost -->
                        {if $_modx->resource.old_price}
                            <div class="project__benefit">
                                <div class="project__benefit--value">
                                    Ваша выгода<br/>
                                    {$price_delta} р.
                                </div>
                                <div class="project__benefit--text">*При заказе до конца месяца</div>
                            </div><!--/.project__benefit -->
                        {else}{/if}

                    </div><!--/.project__row -->
                    <div class="project__params project-params">
                        <ul class="project-params__list">
                            <li class="project-params__item">
                                <span class="project-params__label">Комнат:</span>
                                <span class="project-params__value">{$_modx->resource.cf_room.0}</span>
                                <i class="project-params__icon">
                                    <svg class="ico-svg" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#showcase--room" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </li><!--/.project-params__item -->
                            <li class="project-params__item">
                                <span class="project-params__label">Этажность:</span>
                                <span class="project-params__value">{$_modx->resource.cf_floors.0}</span>
                                <i class="project-params__icon">
                                    <svg class="ico-svg" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#showcase--floor" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </li><!--/.project-params__item -->
                            <li class="project-params__item">
                                <span class="project-params__label">Размеры:</span>
                                <span class="project-params__value">{$_modx->resource.cf_size.0}</span>
                                <i class="project-params__icon">
                                    <svg class="ico-svg" viewBox="0 0 64 64" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#showcase--size" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </li><!--/.project-params__item -->
                            <li class="project-params__item">
                                <span class="project-params__label">Сроки:</span>
                                <span class="project-params__value">{$_modx->resource.cf_term.0} дней</span>
                                <i class="project-params__icon">
                                    <svg class="ico-svg" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#showcase--term" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </li><!--/.project-params__item -->
                        </ul><!--/.project-params__list -->
                    </div><!--/.project-params -->
                </div><!--/.project__main -->

            </div><!--/.project__wrap -->
        </div><!--/.container -->
    </div><!--/.project -->
{/block}

{block 'main'}

    <div class="project-price"
         data-price="{$_modx->resource.price}"
         data-walls="{$_modx->resource.price_1_01}"
         data-foundation="0"
         data-roof="0"
    </div>

    <div class="project-options">
        <div class="container">
            <div class="project-options__main">
                <div class="project-options__group project-options__group--one">
                    <div class="project-options__title">Брус естественной влажности</div><!--/.project-options__title -->
                    <div class="project-options__content">
                        <div class="project-options__item active" data-price="{$_modx->resource.price_1_01}">
                            <div class="project-options__item--icon">
                                <img src="assets/template/images/project_options__01.webp" class="img-fluid" alt="">
                            </div><!--/.project-options__item--icon -->
                            <div class="project-options__item--description">
                                <div class="project-options__item--price">{if $_modx->resource.price_1_01}{$_modx->resource.price_1_01} р.{else}по запросу{/if}</div>
                                <div class="project-options__item--text">Брус нестроганный 150х150мм. Толщина стены 150мм.</div>
                            </div><!--/.project-options__item--description -->
                        </div><!--/.project-options__item -->
                        <div class="project-options__item" data-price="{$_modx->resource.price_1_02}">
                            <div class="project-options__item--icon">
                                <img src="assets/template/images/project_options__02.webp" class="img-fluid" alt="">
                            </div><!--/.project-options__item--icon -->
                            <div class="project-options__item--description">
                                <div class="project-options__item--price">{if $_modx->resource.price_1_02}{$_modx->resource.price_1_02} р.{else}по запросу{/if}</div>
                                <div class="project-options__item--text">Брус нестроганный 150х200мм. Толщина стены 200мм.</div>
                            </div><!--/.project-options__item--description -->
                        </div><!--/.project-options__item -->
                        <div class="project-options__item" data-price="{$_modx->resource.price_1_03}">
                            <div class="project-options__item--icon">
                                <img src="assets/template/images/project_options__03.webp" class="img-fluid" alt="">
                            </div><!--/.project-options__item--icon -->
                            <div class="project-options__item--description">
                                <div class="project-options__item--price">{if $_modx->resource.price_1_03}{$_modx->resource.price_1_03} р.{else}по запросу{/if}</div>
                                <div class="project-options__item--text">Брус профилированный 145х145мм. Толщина стены 145мм. </div>
                            </div><!--/.project-options__item--description -->
                        </div><!--/.project-options__item -->
                        <div class="project-options__item" data-price="{$_modx->resource.price_1_04}">
                            <div class="project-options__item--icon">
                                <img src="assets/template/images/project_options__04.webp" class="img-fluid" alt="">
                            </div><!--/.project-options__item--icon -->
                            <div class="project-options__item--description">
                                <div class="project-options__item--price">{if $_modx->resource.price_1_04}{$_modx->resource.price_1_04} р.{else}по запросу{/if}</div>
                                <div class="project-options__item--text">Брус профилированный 145х195мм. Толщина стены 195мм.</div>
                            </div><!--/.project-options__item--description -->
                        </div><!--/.project-options__item -->
                    </div><!--/.project-options__content -->
                </div><!--/.project-options__group -->
                <div class="project-options__group project-options__group--two">
                    <div class="project-options__title">Фундамент</div><!--/.project-options__title -->
                    <div class="project-options__content">
                        <div class="project-options__item" data-price="{$_modx->resource.price_4_01}">
                            <div class="project-options__item--icon">
                                <img src="assets/template/images/project_options__05.webp" class="img-fluid" alt="">
                            </div><!--/.project-options__item--icon -->
                            <div class="project-options__item--description">
                                <div class="project-options__item--price">{if $_modx->resource.price_4_01}{$_modx->resource.price_4_01} р.{else}по запросу{/if}</div>
                                <div class="project-options__item--title">Свайно-винтовой</div>
                                <div class="project-options__item--text">Диаметр 108мм, длинна 2.5м, толщина стенки 4мм, оголовок 200х200мм</div>
                            </div><!--/.project-options__item--description -->
                        </div><!--/.project-options__item -->
                        <div class="project-options__item" data-price="{$_modx->resource.price_4_02}">
                            <div class="project-options__item--icon">
                                <img src="assets/template/images/project_options__06.webp" class="img-fluid" alt="">
                            </div><!--/.project-options__item--icon -->
                            <div class="project-options__item--description">
                                <div class="project-options__item--price">{if $_modx->resource.price_4_02}{$_modx->resource.price_4_02} р.{else}по запросу{/if}</div>
                                <div class="project-options__item--title">Ленточный мелкозаглубленный</div>
                                <div class="project-options__item--text">Ширина ленты 30см, глубина 50см, высота 50см</div>
                            </div><!--/.project-options__item--description -->
                        </div><!--/.project-options__item -->
                        <div class="project-options__item" data-price="{$_modx->resource.price_4_03}">
                            <div class="project-options__item--icon">
                                <img src="assets/template/images/project_options__07.webp" class="img-fluid" alt="">
                            </div><!--/.project-options__item--icon -->
                            <div class="project-options__item--description">
                                <div class="project-options__item--price">{if $_modx->resource.price_4_03}{$_modx->resource.price_4_03} р.{else}по запросу{/if}</div>
                                <div class="project-options__item--title">ЖБ Сваи</div>
                                <div class="project-options__item--text">Сечение 150х150мм, длина 3 метра, оголовок 200х200мм</div>
                            </div><!--/.project-options__item--description -->
                        </div><!--/.project-options__item -->
                    </div><!--/.project-options__content -->
                </div><!--/.project-options__group -->
                <div class="project-options__group project-options__group--three">
                    <div class="project-options__title">Кровля</div><!--/.project-options__title -->
                    <div class="project-options__content project-options__content--column">
                        <div class="project-options__item" data-price="{$_modx->resource.price_5_02}">
                            <div class="project-options__item--icon">
                                <img src="assets/template/images/project_options__08.webp" class="img-fluid" alt="">
                            </div><!--/.project-options__item--icon -->
                            <div class="project-options__item--description">
                                <div class="project-options__item--price">{if $_modx->resource.price_5_02}{$_modx->resource.price_5_02} р.{else}по запросу{/if}</div>
                                <div class="project-options__item--title">Металлочерепица</div>
                                <div class="project-options__item--text">
                                    Гранд Лайн 0.45мм.<br/>
                                    Цвет по выбору
                                </div>
                            </div><!--/.project-options__item--description -->
                        </div><!--/.project-options__item -->
                        <div class="project-options__item" data-price="{$_modx->resource.price_5_01}">
                            <div class="project-options__item--icon">
                                <img src="assets/template/images/project_options__09.webp" class="img-fluid" alt="">
                            </div><!--/.project-options__item--icon -->
                            <div class="project-options__item--description">
                                <div class="project-options__item--price">{if $_modx->resource.price_5_01}{$_modx->resource.price_5_01} р.{else}по запросу{/if}</div>
                                <div class="project-options__item--title">Ондулин</div>
                            </div><!--/.project-options__item--description -->
                        </div><!--/.project-options__item -->
                    </div><!--/.project-options__content -->
                </div><!--/.project-options__group -->
            </div><!--/.project-options__main -->
            <div class="project-options__checkout">
                <div class="project-options__total">
                    <div class="project-options__total--label">Итого:</div>
                    <div class="project-options__total--value">{if $_modx->resource.price}<span class="project-summary">{$_modx->resource.price}</span> р.{else}по запросу{/if}</div>
                </div>
                <div class="project-options__button">
                    <a href="#order" type="button" class="btn project-checkout btn-modal">Оформить заказ</a>
                </div>
            </div>
        </div><!--/.container -->
    </div><!--/.project-options -->

    <div class="info">
        <div class="container">
            <div class="tabs">
                <div class="tabs__header">
                    <ul class="tabs__list">
                        <li class="tabs__item">
                            <button class="tabs__btn tabs__btn--active" data-tabs-path="tab1">Комплектация</button>
                        </li>
                        <li class="tabs__item">
                            <button class="tabs__btn" data-tabs-path="tab2">Фундамент</button>
                        </li>
                        <li class="tabs__item">
                            <button class="tabs__btn" data-tabs-path="tab3">Кровля</button>
                        </li>
                        <li class="tabs__item">
                            <button class="tabs__btn" data-tabs-path="tab4">Оплата и доставка</button>
                        </li>
                    </ul><!-- /.tabs__list -->
                </div><!-- /.tabs__header -->
                <div class="tabs__content tabs__content--active" data-tabs-target="tab1">
                    <div class="info__table">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <td>
                                        <div class="info__table--title">
                                            <a href="assets/template/images/content/project__01.webp" class="info-view btn-modal">
                                                <i class="info-view__image">
                                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#photo-camera" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                                <i class="info-view__loop">
                                                    <i class="info-view__loop--icon">
                                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#magnifying-glass" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                </i>
                                            </a>
                                            <div class="info__table--name">Фундамент</div>
                                        </div>
                                    </td>
                                    <td>В базовую стоимость не входит, рассчитывается отдельно. Возможные варианты: опорно-столбчатый из бетонных блоков, свайно-винтовой, ленточный, монолитная плита.</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="info__table--title">
                                            <a href="assets/template/images/content/project__01.webp" class="info-view btn-modal">
                                                <i class="info-view__image">
                                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#photo-camera" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                                <i class="info-view__loop">
                                                    <i class="info-view__loop--icon">
                                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#magnifying-glass" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                </i>
                                            </a>
                                            <div class="info__table--name">Основание (обвязка)</div>
                                        </div>
                                    </td>
                                    <td>Обвязка - 2 нижних венца из нестроганного бруса. Толщина сечения согласно выбранной толщины наружных стен (100х150 мм, 150х150 мм или 200х150 мм).</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="info__table--title">
                                            <a href="assets/template/images/content/project__01.webp" class="info-view btn-modal">
                                                <i class="info-view__image">
                                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#photo-camera" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                                <i class="info-view__loop">
                                                    <i class="info-view__loop--icon">
                                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#magnifying-glass" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                </i>
                                            </a>
                                            <div class="info__table--name">Половые лаги</div>
                                        </div>
                                    </td>
                                    <td>Обрезной брус 50х150 мм с шагом не более 600 мм.</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="info__table--title">
                                            <a href="assets/template/images/content/project__01.webp" class="info-view btn-modal">
                                                <i class="info-view__image">
                                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#photo-camera" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                                <i class="info-view__loop">
                                                    <i class="info-view__loop--icon">
                                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#magnifying-glass" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                </i>
                                            </a>
                                            <div class="info__table--name">Черновой пол</div>
                                        </div>
                                    </td>
                                    <td>Обрезная доска 1-го сорта 22х100 мм естественной влажности, укладывается на брусок 40х50 мм, прибитый к лагам.</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="info__table--title">
                                            <a href="assets/template/images/content/project__01.webp" class="info-view btn-modal">
                                                <i class="info-view__image">
                                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#photo-camera" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                                <i class="info-view__loop">
                                                    <i class="info-view__loop--icon">
                                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#magnifying-glass" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                </i>
                                            </a>
                                            <div class="info__table--name">Наружные стены</div>
                                        </div>
                                    </td>
                                    <td>Профилированный брус 90х140, 140х140 или 190х140 мм. Форма сечения на выбор: прямоугольная или закругленная с внешней стороны.</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="info__table--title">
                                            <a href="assets/template/images/content/project__01.webp" class="info-view btn-modal">
                                                <i class="info-view__image">
                                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#photo-camera" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                                <i class="info-view__loop">
                                                    <i class="info-view__loop--icon">
                                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#magnifying-glass" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                </i>
                                            </a>
                                            <div class="info__table--name">Сборка сруба</div>
                                        </div>
                                    </td>
                                    <td>Между венцами проф. бруса прокладывается льноджутовый утеплитель. Сборка осуществляется на металлические нагеля (гвозди 200 мм). В качестве дополнительной услуги - сборка на берёзовые нагеля.</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="info__table--title">
                                            <a href="assets/template/images/content/project__01.webp" class="info-view btn-modal">
                                                <i class="info-view__image">
                                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#photo-camera" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                                <i class="info-view__loop">
                                                    <i class="info-view__loop--icon">
                                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#magnifying-glass" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                </i>
                                            </a>
                                            <div class="info__table--name">Стыковка угловых соединений</div>
                                        </div>
                                    </td>
                                    <td>Рубка наружных стен и внутренних перегородок осуществляется в "тёплый угол" (шип-паз). Акция: БЕСПЛАТНО!</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="info__table--title">
                                            <a href="assets/template/images/content/project__01.webp" class="info-view btn-modal">
                                                <i class="info-view__image">
                                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#photo-camera" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                                <i class="info-view__loop">
                                                    <i class="info-view__loop--icon">
                                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#magnifying-glass" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                </i>
                                            </a>
                                            <div class="info__table--name">Перегородки</div>
                                        </div>
                                    </td>
                                    <td>Прямой профилированный брус из древесины хвойных пород естественной влажности 90х140 мм.</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="info__table--title">
                                            <a href="assets/template/images/content/project__01.webp" class="info-view btn-modal">
                                                <i class="info-view__image">
                                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#photo-camera" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                                <i class="info-view__loop">
                                                    <i class="info-view__loop--icon">
                                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#magnifying-glass" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                </i>
                                            </a>
                                            <div class="info__table--name">Высота потолков</div>
                                        </div>
                                    </td>
                                    <td>1-го этажа: 2.35 м (17 венцов), 2-го этажа: 2.3 м.</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="info__table--title">
                                            <a href="assets/template/images/content/project__01.webp" class="info-view btn-modal">
                                                <i class="info-view__image">
                                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#photo-camera" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                                <i class="info-view__loop">
                                                    <i class="info-view__loop--icon">
                                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#magnifying-glass" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                </i>
                                            </a>
                                            <div class="info__table--name">Потолочные балки</div>
                                        </div>
                                    </td>
                                    <td>Нестроганная доска естественной влажности 40х100 мм с шагом не более 900 мм.</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="info__table--title">
                                            <a href="assets/template/images/content/project__01.webp" class="info-view btn-modal">
                                                <i class="info-view__image">
                                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#photo-camera" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                                <i class="info-view__loop">
                                                    <i class="info-view__loop--icon">
                                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#magnifying-glass" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                </i>
                                            </a>
                                            <div class="info__table--name">Стропила</div>
                                        </div>
                                    </td>
                                    <td>Нестроганная доска естественной влажности 40х100 мм с шагом не более 900 мм.</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="info__table--title">
                                            <a href="assets/template/images/content/project__01.webp" class="info-view btn-modal">
                                                <i class="info-view__image">
                                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#photo-camera" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                                <i class="info-view__loop">
                                                    <i class="info-view__loop--icon">
                                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#magnifying-glass" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                </i>
                                            </a>
                                            <div class="info__table--name">Обрешётка крыши</div>
                                        </div>
                                    </td>
                                    <td>Одинарная, из обрезной доски естественной влажности 22 мм.</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="info__table--title">
                                            <a href="assets/template/images/content/project__01.webp" class="info-view btn-modal">
                                                <i class="info-view__image">
                                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#photo-camera" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                                <i class="info-view__loop">
                                                    <i class="info-view__loop--icon">
                                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#magnifying-glass" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                </i>
                                            </a>
                                            <div class="info__table--name">Крыша и кровля</div>
                                        </div>
                                    </td>
                                    <td>
                                        Крыша двускатная или ломаная, согласно проекту. Высота конька - 3.5 м. Кровельный материал - ондулин (красный, коричневый или зеленый). Прокладывается ветроизоляционная мембрана "Изоспан А".
                                        Замена на металлочерепицу - бесплатно при заказе от 770 т.р.
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="info__table--title">
                                            <a href="assets/template/images/content/project__01.webp" class="info-view btn-modal">
                                                <i class="info-view__image">
                                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#photo-camera" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                                <i class="info-view__loop">
                                                    <i class="info-view__loop--icon">
                                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#magnifying-glass" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                </i>
                                            </a>
                                            <div class="info__table--name">Внешние фронтоны и поднебесники</div>
                                        </div>
                                    </td>
                                    <td>Фронтоны каркасные с прокладкой ветрозащитной мембраны "Изоспан А". Снаружи обшиваются вагонкой 14 мм класса "В" естественной влажности. Поднебесники по скату и по фронтону шириной 270 мм.</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="info__table--title">
                                            <a href="assets/template/images/content/project__01.webp" class="info-view btn-modal">
                                                <i class="info-view__image">
                                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#photo-camera" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                                <i class="info-view__loop">
                                                    <i class="info-view__loop--icon">
                                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#magnifying-glass" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                </i>
                                            </a>
                                            <div class="info__table--name">Оконные и дверные проёмы</div>
                                        </div>
                                    </td>
                                    <td>Во всех оконных и дверных проёмах оставляются перевязочные венцы для более равномерной усадки сруба.</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="info__table--title">
                                            <a href="assets/template/images/content/project__01.webp" class="info-view btn-modal">
                                                <i class="info-view__image">
                                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#photo-camera" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                                <i class="info-view__loop">
                                                    <i class="info-view__loop--icon">
                                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#magnifying-glass" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                </i>
                                            </a>
                                            <div class="info__table--name">Компенсаторы усадки</div>
                                        </div>
                                    </td>
                                    <td>В опоры столбов устанавливаются металлические винтовые компенсаторы усадки. Акция: БЕСПЛАТНО!</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="info__table--title">
                                            <a href="assets/template/images/content/project__01.webp" class="info-view btn-modal">
                                                <i class="info-view__image">
                                                    <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="assets/template/images/sprites/sprite-icons.svg#photo-camera" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </i>
                                                <i class="info-view__loop">
                                                    <i class="info-view__loop--icon">
                                                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="assets/template/images/sprites/sprite-icons.svg#magnifying-glass" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                </i>
                                            </a>
                                            <div class="info__table--name">Фундамент</div>
                                        </div>
                                    </td>
                                    <td>
                                        В базовую стоимость не входит, рассчитывается отдельно. Возможные варианты: опорно-столбчатый из бетонных блоков, свайно-винтовой, ленточный, монолитная плита.<br/>
                                        Выбрать фундамент.
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div><!-- /.info__table -->
                </div><!-- /.tabs__content -->
                <div class="tabs__content" data-tabs-target="tab2">
                    <div class="info__wrap">
                        <div class="info__options">
                            <div class="info__options--item">
                                <div class="info__options--image">
                                    <img src="assets/template/images/info__image_01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="info__options--title">Свайно-винтовой</div>
                                <div class="info__options--price">{if $_modx->resource.price_4_01}от {$_modx->resource.price_4_01} р.{else}по запросу{/if}</div>
                            </div><!--/.info__options--item -->
                            <div class="info__options--item">
                                <div class="info__options--image">
                                    <img src="assets/template/images/info__image_02.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="info__options--title">Ленточный</div>
                                <div class="info__options--price">{if $_modx->resource.price_4_02}от {$_modx->resource.price_4_02} р.{else}по запросу{/if}</div>
                            </div><!--/.info__options--item -->
                            <div class="info__options--item">
                                <div class="info__options--image">
                                    <img src="assets/template/images/info__image_03.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="info__options--title">ЖБ Сваи</div>
                                <div class="info__options--price">{if $_modx->resource.price_4_03}от {$_modx->resource.price_4_03} р.{else}по запросу{/if}</div>
                            </div><!--/.info__options--item -->
                        </div><!--/.info__options -->
                    </div>
                </div><!-- /.tabs__content -->
                <div class="tabs__content" data-tabs-target="tab3">
                    <div class="info__wrap">
                        <div class="info__options">
                            <div class="info__options--item">
                                <div class="info__options--image">
                                    <img src="assets/template/images/info__image_04.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="info__options--title">Металлочерепица</div>
                                <div class="info__options--price">{if $_modx->resource.price_5_02} от {$_modx->resource.price_5_02} р.{else}по запросу{/if}</div>
                            </div><!--/.info__options--item -->
                            <div class="info__options--item">
                                <div class="info__options--image">
                                    <img src="assets/template/images/info__image_06.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="info__options--title">Ондулин</div>
                                <div class="info__options--price">{if $_modx->resource.price_5_01}от {$_modx->resource.price_5_01} р.{else}по запросу{/if}</div>
                            </div><!--/.info__options--item -->
                        </div><!--/.info__options -->
                    </div>
                </div><!-- /.tabs__content -->
                <div class="tabs__content" data-tabs-target="tab4">
                    <div class="info__wrap">
                        <h4>Доставка</h4>

                        <ul>
                            <li>Доставка осуществляется собственным автотранспортом бесплатно до г. Москвы и по Московской области, а так же в радиусе 600 км от г. Великий Новгород.</li>
                            <li>Разгрузка на участке заказчика так же осуществляется бесплатно, при условии возможности подъезда грузового транспорта к участку.</li>
                            <li>Разгрузка выполняется вручную бригадой, либо с помощью спецтехники.</li>
                        </ul>

                        <h4>Доставка</h4>

                        <ul>
                            <li>Доставка осуществляется собственным автотранспортом бесплатно до г. Москвы и по Московской области, а так же в радиусе 600 км от г. Великий Новгород.</li>
                            <li>Разгрузка на участке заказчика так же осуществляется бесплатно, при условии возможности подъезда грузового транспорта к участку.</li>
                            <li>Разгрузка выполняется вручную бригадой, либо с помощью спецтехники.</li>
                        </ul>
                    </div>
                </div><!-- /.tabs__content -->
            </div><!-- /.tabs -->

        </div><!--/.container -->
    </div><!--/.info -->

    {include 'file:templates/common/_visualization.tpl'}

    {include 'file:templates/common/_how-work.tpl'}

    {include 'file:templates/common/_portfolio.tpl'}

    {include 'file:templates/common/_geography.tpl'}

{/block}
